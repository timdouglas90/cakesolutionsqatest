# Cake Solutions QA Technical Test #
### Setup ###

* First you'll need [InteliJ](https://www.jetbrains.com/idea/download/#section=windows) or a similar IDE for Java.
* You'll also need [Maven](https://maven.apache.org/download.cgi) installed and [Setup](https://maven.apache.org/install.html).
* Clone this repo into a directory on your computer.
* Navigate to the directory of this repo in cmd/terminal.
* type the command 
```
#!

mvn test
```
* Followed by the command
```
#!
mvn site
```

* This will start the tests running and also produce a HTML report of the test run.
* The report can be found in... 
```
#!
target/site/surefure-report.html

```
### Improvements ###
* Spell solutions properly (which fails a test) in the page title.
* Add some validation to the registration form to avoid invalid email addresses and names/numbers.
* Send a "Registration Received" email, so the user is aware that the details they have entered are received.
* Change the wording of the registration confirmation to something a bit more professional like "Registration Successful".
* Fix the link on the logo.