package com.cakesolutions.pages;

import java.lang.String;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Driver {

    public static WebDriver driver;

    public static void OpenDriver(){
        driver = new ChromeDriver();
    }

    public static void NavigateToHomepage(String url) {
        driver.navigate().to(url);
    }

    public static void CloseDriver(){
        driver.close();
    }
}
