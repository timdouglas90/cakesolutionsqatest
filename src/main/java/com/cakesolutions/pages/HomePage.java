package com.cakesolutions.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HomePage {

    public static String GetPageTitle(){
        return Driver.driver.getTitle();
    }

    public static void ClickRegistrationButton() {
        WebElement registerButton = Driver.driver.findElement(By.cssSelector(".btn-primary"));
        registerButton.click();
    }

    public static String GetHeader() {
        return Driver.driver.findElement(By.cssSelector("h1")).getText();
    }

    public static void ClickHomeButton() {
        WebElement homeButton = Driver.driver.findElement(By.cssSelector(".img-responsive"));

        homeButton.click();
    }

    public static void ClickRegistrationButtonInHeader() {
        WebElement registrationButtonInHeader = Driver.driver.findElement(By.cssSelector("[href='#/register']"));
        registrationButtonInHeader.click();
    }
}
