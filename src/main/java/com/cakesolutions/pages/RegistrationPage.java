package com.cakesolutions.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RegistrationPage {

    public static String GetHeaderText(){
        WebElement registrationFormTitle = Driver.driver.findElement(By.cssSelector(".signup-screen h2"));
        return registrationFormTitle.getText();
    }

    public static void EnterFirstName() {
        WebElement firstName = Driver.driver.findElement(By.id("firstName"));

        firstName.sendKeys("T");
        firstName.sendKeys("D");
        firstName.sendKeys("t");
        firstName.sendKeys("e");
        firstName.sendKeys("s");
        firstName.sendKeys("t");
    }

    public static void EnterLastName() {
        WebElement lastName = Driver.driver.findElement(By.id("lastName"));

        lastName.sendKeys("t");
        lastName.sendKeys("e");
        lastName.sendKeys("s");
        lastName.sendKeys("t");
        lastName.sendKeys("T");
        lastName.sendKeys("D");
    }

    public static void EnterEmail() {
        WebElement email = Driver.driver.findElement(By.id("email"));

        email.sendKeys("T");
        email.sendKeys("D");
        email.sendKeys("t");
        email.sendKeys("e");
        email.sendKeys("s");
        email.sendKeys("t");
        email.sendKeys("@");
        email.sendKeys("t");
        email.sendKeys("e");
        email.sendKeys("s");
        email.sendKeys("t");
        email.sendKeys(".");
        email.sendKeys("c");
        email.sendKeys("o");
        email.sendKeys("m");
    }

    public static void EnterPassword() {
        WebElement password = Driver.driver.findElement(By.id("password"));
        
        password.sendKeys("p");
        password.sendKeys("A");
        password.sendKeys("s");
        password.sendKeys("S");
        password.sendKeys("w");
        password.sendKeys("O");
        password.sendKeys("r");
        password.sendKeys("D");
        password.sendKeys("1");
        password.sendKeys("2");
        password.sendKeys("3");
    }

    public static void ReEnterPassword() {
        WebElement passwordConfirm = Driver.driver.findElement(By.id("passwordConfirm"));

        passwordConfirm.sendKeys("p");
        passwordConfirm.sendKeys("A");
        passwordConfirm.sendKeys("s");
        passwordConfirm.sendKeys("S");
        passwordConfirm.sendKeys("w");
        passwordConfirm.sendKeys("O");
        passwordConfirm.sendKeys("r");
        passwordConfirm.sendKeys("D");
        passwordConfirm.sendKeys("1");
        passwordConfirm.sendKeys("2");
        passwordConfirm.sendKeys("3");
    }

    public static void ClickSubmit() {
        WebElement submitButton = Driver.driver.findElement(By.cssSelector(".btn-primary"));

        submitButton.click();
    }

    public static void ClickCancelButton() {
        WebElement cancelButton = Driver.driver.findElement(By.cssSelector("[href='#/']"));
        cancelButton.click();
    }
}
