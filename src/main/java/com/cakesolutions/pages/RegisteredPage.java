package com.cakesolutions.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RegisteredPage {

    public static String GetRegistrationHeader(){
        WebElement header =  Driver.driver.findElement(By.cssSelector("h2"));

        return header.getText();
    }

    public static void ClickContinue() {
        WebElement continueButton = Driver.driver.findElement(By.cssSelector(".btn-success"));

        continueButton.click();
    }
}
