package com.cakesolutions.pages;

import org.junit.Assert;
import org.junit.Test;
import java.lang.String;

public class FunctionalTest {

    public static String cakesolutionsHomePage = "https://cakesolutions.github.io/cake-qa-test/#/";

    @Test
    public void WhenIGoToTheHomePageTheTitleDisplaysCorrectly(){
        String expected = "Cake Solutions QA Test";

        Driver.OpenDriver();
        Driver.NavigateToHomepage(cakesolutionsHomePage);

        Assert.assertEquals(expected, HomePage.GetPageTitle());

        Driver.CloseDriver();
    }

    @Test
    public void WhenIClickTheHomeButtonIGoHome(){
        String expected = "Cake Solutions QA Test";

        Driver.OpenDriver();
        Driver.NavigateToHomepage(cakesolutionsHomePage);

        HomePage.ClickHomeButton();

        Assert.assertEquals(expected, HomePage.GetPageTitle());

        Driver.CloseDriver();
    }

    @Test
    public void WhenIClickTheRegisterButtonIAmAbleToSuccesfullyRegisterAnAccount(){
        String expectedRegistrationPage = "Registration";
        String expectedRegisteredPage = "Congrats !!";
        String expectedHomePageHeader = "Cake Solutions Ltd";

        Driver.OpenDriver();
        Driver.NavigateToHomepage(cakesolutionsHomePage);
        HomePage.ClickRegistrationButton();

        Assert.assertEquals(expectedRegistrationPage, RegistrationPage.GetHeaderText());

        RegistrationPage.EnterFirstName();
        RegistrationPage.EnterLastName();
        RegistrationPage.EnterEmail();
        RegistrationPage.EnterPassword();
        RegistrationPage.ReEnterPassword();
        RegistrationPage.ClickSubmit();

        Assert.assertEquals(expectedRegisteredPage, RegisteredPage.GetRegistrationHeader());

        RegisteredPage.ClickContinue();

        Assert.assertEquals(expectedHomePageHeader, HomePage.GetHeader());

        Driver.CloseDriver();
    }

    @Test
    public void IAmAbleToCancelRegistration(){
        String expectedHomePageTitle = "Cake Solutions QA Test";
        String expectedRegistrationPageTitle = "Registration";

        Driver.OpenDriver();
        Driver.NavigateToHomepage(cakesolutionsHomePage);

        HomePage.ClickRegistrationButtonInHeader();

        Assert.assertEquals(expectedRegistrationPageTitle, RegistrationPage.GetHeaderText());

        RegistrationPage.ClickCancelButton();

        Assert.assertEquals(expectedHomePageTitle, HomePage.GetPageTitle());

        Driver.CloseDriver();
    }
}
